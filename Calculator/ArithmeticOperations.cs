﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Calculator
{
    class ArithmeticOperations
    {
        public static string SumUp(double firstNumber, double secondNumber)
        {
            try
            {
                //Check if the values aren't 0, because a zero value is gonne stay a zero value.
                if (firstNumber == 0 || secondNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to sum up a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //Round the number en check if the outcome is not negative.
                else if (Math.Ceiling((firstNumber + secondNumber)) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round((firstNumber + secondNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }

        public static string Subtract(double firstNumber, double secondNumber)
        {
            try
            {
                //Check if the values aren't 0, because a zero value is gonne stay a zero value.
                if (firstNumber == 0 || secondNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to subtract a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //Round the number en check if the outcome is not negative.
                else if (Math.Ceiling((firstNumber - secondNumber)) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round((firstNumber - secondNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }

        public static string Multiplication(double firstNumber, double secondNumber)
        {
            try
            {
                //Check if the values aren't 0, because a zero value is gonne stay a zero value.
                if (firstNumber == 0 || secondNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to multiply a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //Round the number en check if the outcome is not negative.
                else if (Math.Ceiling((firstNumber * secondNumber)) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round((firstNumber * secondNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }

        public static string Divide(double firstNumber, double secondNumber)
        {
            try
            {
                //Check if the values aren't 0, because a zero value is gonne stay a zero value.
                if (firstNumber == 0 || secondNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to divide a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //Round the number en check if the outcome is not negative.
                else if (Math.Ceiling((firstNumber / secondNumber)) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round((firstNumber / secondNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }

        public static string Squaring(double sqrtNumber)
        {
            try
            {
                //Check if the value isn't 0, because a zero value is gonne stay a zero value.
                if (sqrtNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to square a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //.NET provides a 'IsNaN' method if a sqrt is negative or zero.
                else if (Double.IsNaN(Math.Sqrt(sqrtNumber)) || Math.Sqrt(sqrtNumber) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0 or the square value is negative.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round(Math.Sqrt(sqrtNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }

        public static string Exponentiation(double firstNumber, double secondNumber)
        {
            try
            {
                //Check if the values aren't 0, because a zero value is gonne stay a zero value.
                if (firstNumber == 0 || secondNumber == 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("Unable to exponentiate a zero value");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "ZERO VALUE";
                }
                //Round the number en check if the outcome is not negative.
                else if (Math.Ceiling(Math.Pow(firstNumber, secondNumber)) < 0)
                {
                    Console.WriteLine("============== MESSAGE FROM THE SYSTEM ============== ");
                    Console.WriteLine("We are unable to give you a answer when the total value is bellow 0.");
                    Console.WriteLine("============== END MESSAGE FROM THE SYSTEM ============== ");
                    return "SO OUTPUT = 0";
                }
                else
                {
                    //Round the number on 2 decimals and round it away from the zero. This will return to the switch and prints out the outcome.
                    return Math.Round(Math.Pow(firstNumber, secondNumber), 2, MidpointRounding.AwayFromZero).ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPECTED ISSUE";
            }
        }
    }
}
