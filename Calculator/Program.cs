﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=======================================================================");
            Console.WriteLine("WELCOME TO THE CALCULATION TOOL BY");
            Console.WriteLine("Maurice, Ryan and Yeray - TEAM A3-2");
            Console.WriteLine("=======================================================================");
            startProgram();
        }

        private static void startProgram()
        {
            //Give the user options to choose from.
            Console.WriteLine("Choose one of the following options:");
            Console.WriteLine("1: Sum up.");
            Console.WriteLine("2: Subtract.");
            Console.WriteLine("3: Multiply.");
            Console.WriteLine("4: Divide.");
            Console.WriteLine("5: Squaring.");
            Console.WriteLine("6: Exponentiate.");
            string calculationOption = Console.ReadLine();
            try
            {
                //Catch a option and fire the switch.
                //Each option has a method inside called ShowResult this will run another switch statement to fire a method in the class ArithmeticOperations.
                switch (calculationOption)
                {
                    case "1":
                        Console.Write("Enter your first number: ");
                        string firstNumber = Console.ReadLine();
                        Console.Write("Enter your second number: ");
                        string secondNumber = Console.ReadLine();
                        Console.WriteLine("======================================================================= +");
                        Console.WriteLine(ShowResult("1", firstNumber, secondNumber));
                        startAgain();
                        return;
                    case "2":
                        Console.Write("Enter your first number: ");
                        string firstNumberO2 = Console.ReadLine();
                        Console.Write("Enter your second number: ");
                        string secondNumberO2 = Console.ReadLine();
                        Console.WriteLine("======================================================================= -");
                        Console.WriteLine(ShowResult("2", firstNumberO2, secondNumberO2));
                        startAgain();
                        return;
                    case "3":
                        Console.Write("Enter your first number: ");
                        string firstNumberO3 = Console.ReadLine();
                        Console.Write("Enter your second number: ");
                        string secondNumberO3 = Console.ReadLine();
                        Console.WriteLine("======================================================================= *");
                        Console.WriteLine(ShowResult("3", firstNumberO3, secondNumberO3));
                        startAgain();
                        return;
                    case "4":
                        Console.Write("Enter your first number: ");
                        string firstNumberO4 = Console.ReadLine();
                        Console.Write("Enter your second number: ");
                        string secondNumberO4 = Console.ReadLine();
                        Console.WriteLine("======================================================================= /");
                        Console.WriteLine(ShowResult("4", firstNumberO4, secondNumberO4));
                        startAgain();
                        return;
                    case "5":
                        Console.Write("Enter your number to square: ");
                        string firstNumberO5 = Console.ReadLine();
                        Console.WriteLine($"======================================================================= square({firstNumberO5})");
                        Console.WriteLine(ShowResult("5", firstNumberO5, String.Empty));
                        startAgain();
                        return;
                    case "6":
                        Console.Write("Enter your first number: ");
                        string firstNumberO6 = Console.ReadLine();
                        Console.Write("Enter your second number: ");
                        string secondNumberO6 = Console.ReadLine();
                        Console.WriteLine("======================================================================= ^");
                        Console.WriteLine(ShowResult("6", firstNumberO6, secondNumberO6));
                        startAgain();
                        return;
                    default:
                        Console.WriteLine("There is no option with number: '" + calculationOption + "'" + ".");
                        startAgain();
                        return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
            }
        } 

        private static string ShowResult(string option, string firstNumber, string secondNumber)
        {
            try
            {
                switch (option)
                {
                    //Convert the given string to double. Fire a ArithmeticOperation.
                    case "1":
                        return ArithmeticOperations.SumUp(Convert.ToDouble(firstNumber), Convert.ToDouble(secondNumber));
                    case "2":
                        return ArithmeticOperations.Subtract(Convert.ToDouble(firstNumber), Convert.ToDouble(secondNumber));
                    case "3":
                        return ArithmeticOperations.Multiplication(Convert.ToDouble(firstNumber), Convert.ToDouble(secondNumber));
                    case "4":
                        return ArithmeticOperations.Divide(Convert.ToDouble(firstNumber), Convert.ToDouble(secondNumber));
                    case "5":
                        return ArithmeticOperations.Squaring(Convert.ToDouble(firstNumber));
                    case "6":
                        return ArithmeticOperations.Exponentiation(Convert.ToDouble(firstNumber), Convert.ToDouble(secondNumber));
                    default:
                        return "NO AVAILABLE OPTION";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an unexpted issue, see next line.");
                Console.WriteLine("============== ERROR MESSAGE ============== ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("============== END ERROR MESSAGE ============== ");
                return "UNEXPTED ISSUE";
            }
        }

        private static void startAgain()
        {
            //To options to do more calculation this will run everytime after a calcution until the user enter 'n' to exit the application.
            Console.WriteLine("=======================================================================");
            Console.Write("MORE CALCULATIONS? y = yes, n = no: ");
            string again = Console.ReadLine();
            switch (again)
            {
                case "y":
                    startProgram();
                    break;
                case "n":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("No option selected exiting program...");
                    Environment.Exit(1);
                    break;
            }
        }
    }
}
